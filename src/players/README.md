# Veloren for Players

Thank you for your interest in playing the Game!

If you want to kickstart your playthrough you can read through the [Getting started](getting-started/) guide.
As the game often updates several times a day we currently recommend using [Airshipper](airshipper.md) the official Veloren launcher.

Much fun playing the game!
